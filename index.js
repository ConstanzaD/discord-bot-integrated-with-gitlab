const { Client, GatewayIntentBits } = require('discord.js')
require('dotenv/config')

const client = new Client({
    intents:[
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.GuildIntegrations,
        GatewayIntentBits.GuildMessageTyping,
        GatewayIntentBits.GuildMessageReactions,
        GatewayIntentBits.GuildWebhooks,
        GatewayIntentBits.MessageContent,
        GatewayIntentBits.DirectMessages,
        GatewayIntentBits.DirectMessageTyping,
        GatewayIntentBits.DirectMessageReactions,
        
        
        
    ]
})
client.on('ready', () => {
    console.log('the bot is ready')
});

client.on('messageCreate', (message) => {
    if (message.content === 'Cuantas horas llevo?'){
        
        message.reply('Llevas 30 horas esta semana, buen trabajo!')
    }
});
client.login(process.env.TOKEN)
